import { useMemo, useState, useRef} from "react";

export default function UseMemoComponent(){
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [product, setProduct] = useState([]);

    const nameRef  = useRef();

    const handlerSubmit = () => {
        setProduct([...product, {
            name, 
            price: +price
        }]);
        setName('');
        setPrice("");
        nameRef.current.focus()
    }

    const total = useMemo(() => {
        const result = product.reduce((result, props) => {
            console.log("Tính toàn lại")
            return result + props.price
        }, 0)

        return result
    }, [product]) //dependency là product, chỉ khi product thay đổi thì hàm mới thực hiện việc tính toán

    //hàm sẽ chạy lại khi người dùng nhập và tính tổng 
    //khiến việc tính toán bị dư thừa
    // const  total  = product.reduce(function( results, prods) {
    //     console.log("Tính toàn lại")
    //     return results + prods.price;
        
    // }, 0 )
    return (
        <div style={{marginTop: "20px"}}>
            <code style={{color:"red"}}>/  useMemo Example /</code>
            <br />
            <input style={{marginTop: "10px"}}
                ref= {nameRef}
                value={name}
                placeholder = "Enter name ..."
                onChange={ e => setName(e.target.value)}
            />
            <br/>
            <input style={{marginTop: "10px"}}
                value={price}
                placeholder = "Enter Price ..."
                onChange={ e => setPrice(e.target.value)}
            />
            <br/>
            <button style={{marginTop: "10px"}} onClick={handlerSubmit}>Add</button>
            <br />
            Product: {total}
            <ul>
                {
                    product.map((product, index) => (
                        <li key={index}>{product.name} - {product.price}</li>
                    ))
                }
            </ul>
        </div>
    )
}
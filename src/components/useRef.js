import { useRef, useState, useEffect} from "react";

const UseRefComponent = () => {
    const [count, setCount] = useState(60);

    const timerId = useRef();
    const prevNumber= useRef();

    // Khi tải trang component sẽ chạy xuống phần return đầu tiên và sau đó useEffect mới bắt đầu được gọi 
    // khi này giá trị của biên prevnumber sẽ được add value của count sau khi tải trang 
    // khi hàm hangleStart được chạy => mỗi lần count thay đổi thì trang sẽ re-render
    // khi này console.log 2 biến: timerId sẽ có giá trị mới của count, còn prevNumber sẽ có giá trị cũ của count (vì useEffect được gọi cuỗi cùng khi tải trang, nên lúc console.log prevNumber chưa thay đổi giá trị);
    useEffect(() => {

        prevNumber.current  = count

    }, [count])

    const handleStart = () => {

        timerId.current = setInterval(() => {
            setCount((prevCount) => prevCount - 1);
        }, 1000)
        
        console.log("Start ->", timerId.current)
    }

    const handleStop = () => {

        clearInterval(timerId.current);
        console.log("Stop ->", timerId.current)

    }
    console.log(count, prevNumber.current);
    return(
        <div style={{marginTop: "20px"}}>
            <code style={{color:"red"}}>/ useRef example /</code>
            <h1 style={{margin: "10px"}}>{count}</h1>
            <button onClick={handleStart}>Start</button>
            <button onClick={handleStop}>Stop</button>
        </div>
    )
}

export default UseRefComponent
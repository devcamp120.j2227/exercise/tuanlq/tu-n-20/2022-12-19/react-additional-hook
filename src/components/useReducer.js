import { useReducer} from "react";

//Initital state
const initialState = 0;

//action
const PLUS_ACTION = "Cộng thêm 1";
const MINUS_ACTION = "Trừ đi 1";

//reducer
const reducer =  (state, action) => {
    switch(action){
        case PLUS_ACTION:
            return state + 1;
        case MINUS_ACTION:
            return state -1;
        default: throw new Error("Action is not valid!");
    }
}
export default function UseReducerComponent (){
    const [count, dispatch] = useReducer(reducer, initialState);
    
    return(
        <div>
            <code style={{color:"red"}}>/ useReducer Example /</code>
            <h2>{count}</h2>

            <button onClick={() => dispatch(PLUS_ACTION)}>plus</button>

            <button onClick={() => dispatch(MINUS_ACTION)}>minus</button>
        </div>
    )

}
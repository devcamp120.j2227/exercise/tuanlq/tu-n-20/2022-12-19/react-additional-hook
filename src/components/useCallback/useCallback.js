import { useCallback, useState } from "react";
import Content1 from "./content";
export default function UseCallbackComponent () {
    const [count, setCount] = useState(0);

    // const handleIncrease = () => {
    //     setCount(prevCount => prevCount + 1)
    // }
    const handleIncrease = useCallback(() => {
        setCount(prevCount => prevCount + 1)
    }, [])
    return (
        <div >
            <Content1 onIncrease = {handleIncrease}></Content1>
            <h1>{count}</h1>
        </div>
    )
}
import { memo } from "react";

 function Content1 ({onIncrease}){

    console.log('re-render');

    return(
        <>
            <code style={{color:"red"}}>/ useCallback Example /</code>
            <br />
            <button onClick={onIncrease}>Click me!</button>
        </>
    )
}
export default memo(Content1)
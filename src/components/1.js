import React from "react";


export default class ClassRoom extends React.Component{
    constructor(props){
        super(props);

        this.state = {studentsCount : 0};
        this.addStudent = this.addStudent.bind(this);
    }
    
    addStudent () {
        this.setState((state)=>{
            return {studentsCount: state.studentsCount + 1}
        });
    }
    
    render(){
        return(
            <div>
                <code style={{color:"red"}}>/ Class Component /</code>
                <p style={{padding: 0}}>Number of students in class room: {this.state.studentsCount}</p>
                <button onClick={this.addStudent}>Add Student</button>
            </div>
        )
    }
}
import { useState, createContext } from "react";

const ThemeContext = createContext();

function ContextProvider ({children}) {
    const [theme, setTheme] = useState("Light");
    const handlerSwitchToggle = () => {
        setTheme(theme === "Light" ?  "Dark" : "Light")
    }
    const objectContext = {
        theme,
        handlerSwitchToggle
    }
    return(
        <ThemeContext.Provider value={objectContext}>
            {children}
        </ThemeContext.Provider>
    )
}

export {ThemeContext, ContextProvider}
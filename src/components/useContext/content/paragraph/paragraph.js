import "./paragraph.css"
import { useContext } from "react"
import { ThemeContext } from "../../contextProvider"
// const Paragraph1 = ({theme}) => {
//     console.log(`theme: ${theme}`)
//     return(
//         <div>
//             <p className={theme} >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis perferendis ipsa dolores facere sequi nobis aliquam itaque, nam necessitatibus non earum obcaecati. Fuga delectus aliquid recusandae quos. Officia, et libero.</p>
//         </div>
//     )
// }

const Paragraph1 = () => {
    const theme =  useContext(ThemeContext)
    console.log(`theme: ${theme.theme}`)
    return(
        <div>
            <p className={theme.theme} >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis perferendis ipsa dolores facere sequi nobis aliquam itaque, nam necessitatibus non earum obcaecati. Fuga delectus aliquid recusandae quos. Officia, et libero.</p>
        </div>
    )
}

export default Paragraph1
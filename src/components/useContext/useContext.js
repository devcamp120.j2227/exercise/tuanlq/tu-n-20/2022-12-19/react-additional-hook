
import Content from "./content/content";
import { useContext } from "react";
import { ThemeContext } from "./contextProvider";
//B1: Create Context
// export const ThemeContext =  createContext();

// const UseContextComponent =() => {

//     //Component A =>  Component B =>  Compnent C

//     //B1: Create Context
//     //B2: Provider
//     //B3: Consumer
//     const [theme, setTheme] = useState("Light");
//     const handlerSwitchToggle = () => {
//         setTheme(theme === "Light" ?  "Dark" : "Light")
//     }
//     return(
//         // B2: Provider
//         <ThemeContext.Provider value={theme}> 
//             <div style={{padding:"20px 0"}}>
//                 <code>/ useContext Hook Example / </code>
//                 <Content theme = {theme}></Content>
//                 <button onClick={handlerSwitchToggle}>Toggle Theme</button>
//             </div>
//         </ThemeContext.Provider>
//     )
// }

const UseContextComponent =() => {
    const handlerClick = useContext(ThemeContext)
    return(
            <div style={{padding:"20px 0"}}>
                <code style={{color:"red"}}>/ useContext Hook Example / </code>
                <Content></Content>
                <button onClick={handlerClick.handlerSwitchToggle}>Toggle Theme</button>
            </div>
    )
}

export default UseContextComponent
import ClassRoom from "./components/1";
import UseRefComponent from "./components/useRef";
import { ContextProvider } from "./components/useContext/contextProvider";
import UseContextComponent from "./components/useContext/useContext";
import UseReducerComponent from "./components/useReducer";
import UseMemoComponent from "./components/useMemo";
import UseCallbackComponent from "./components/useCallback/useCallback";
function App() {
  return (
    <div style={{padding: "20px"}}>
      <ClassRoom></ClassRoom>
      <UseRefComponent></UseRefComponent>
      <ContextProvider>
        <UseContextComponent></UseContextComponent>
      </ContextProvider>
      <UseReducerComponent></UseReducerComponent>
      <UseMemoComponent></UseMemoComponent>
      <UseCallbackComponent></UseCallbackComponent>
    </div>
  );
}

export default App;
